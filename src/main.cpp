/* 
 * File:   main.cpp
 * Author: Kevin
 *
 * Created on 13 December 2013, 20:23
 */

#include <cstdlib>
#include <iostream>
#include "Mathematics/Float.h"
#include "Mathematics/Vector3.h"
#include "Mathematics/BoundedRay.h"
#include "Shape/IBoundingShape.h"
#include "Mathematics/Constants.h"
#include "Shape/Intersection.h"
#include "Shape/Cylinder.h"
#include "Shape/Triangle.h"
#include "Camera/ThinLensCamera.h"
#include "Camera/PinholeCamera.h"
#include "Camera/RealPinholeCamera.h"
#include "Shape/Sphere.h"
#include "Shape/Plane.h"
#include "Shape/AxisAlignedBoundingBox.h"
#include "Mathematics/TMatrix.h"
#include "Mathematics/Matrix.h"
#include "Spectrum/Spectrum.h"
//#include "../src/Mathematics/TMatrix.cpp"
#include "Mathematics/Algebra/DiscreteFunction.h"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

bool checkRequirements()
{
    return Candela::Mathematics::Float::IEEE754;
}

void toPPM(unsigned dimx, unsigned dimy, unsigned * buff){
    unsigned i, j;
    FILE *fp = fopen("first.ppm", "wb"); /* b - binary mode */
    fprintf(fp, "P6\n%d %d\n255\n", dimx, dimy);
    for (j = 0; j < dimy; ++j)
    {
      for (i = 0; i < dimx; ++i)
      {
        unsigned char color[3];
        color[0] = (buff[j * dimx + i] >> 16) & 0xFF;
        color[1] = (buff[j * dimx + i] >> 8) & 0xFF;
        color[2] = (buff[j * dimx + i] >> 0) & 0xFF;
        fwrite(color, 1, 3, fp);
      }
    }
    fclose(fp);
}

void testRender()
{
    using namespace Candela::Mathematics;
    //setup camera
    Candela::Camera::PinholeCamera pc ( Vector::UnitZ ,
            Candela::Camera::CameraPlane( Rectangle_F (36E-3f, 24E-3f), 70E-3f ) ,
            Rectangle_I(0, 900, 0, 600) ) ;
    //pc.setApertureSize(0.5E-3f);
    //pc.setFNumber(3.3f);
    //pc.setObjectPlaneDistance(0.5f);
    //pc.lookAt(Vector::UnitZ, Vector::UnitY + Vector::UnitX * 0.25f);
    
    float z = 0.25f;
    Candela::Shape::Triangle t ( Vector(-0.04f, -0.04f,z+0.02f), Vector(0.04f,-0.04f,z+0.02f) , Vector(0.04f,0.04f,z+0.02f));
    //Candela::Shape::Sphere t (Vector(0.f, 0.f, z + 0.02f), 0.04f);
    //Candela::Shape::Cylinder t (Vector(0.f, 0.f, z + 0.02f), (~(-Vector::UnitX + Vector::UnitZ + Vector::UnitY)) * 0.01f, 0.02f);
    //Candela::Shape::Plane t (Vector(0.f, 0.f, z + 0.02f), Vector(1, 1, -0.2f));
    //Candela::Shape::AxisAlignedBoundingBox t (Vector(0.f, 0.f, z + 0.06f), 0.04f);
    //internal
    //Candela::Shape::Sphere t (Vector(0.f, 0.f, 0.f), 0.04f);
    //Candela::Shape::AxisAlignedBoundingBox t (Vector(0.f, 0.f, 0.f), 0.04f);
    Candela::Shape::Intersection intersection;
    BoundedRay ray(0.f, 0.f);
    
    unsigned (*buff)[600][900] = ( unsigned (*)[600][900] )new unsigned[600 * 900]();
    unsigned char (* const cBuff)[600][900][sizeof(unsigned)] = (unsigned char (*)[600][900][sizeof(unsigned)])buff;
    unsigned temp;
    for(unsigned y = 0; y < 600; ++y)
    {
        for(unsigned x = 0; x < 900; ++x)
        {
            float temp = 0.f;
            unsigned pSamples = 1;
            //unsigned lSamples = 64;
            unsigned lSizeX = 1;
            unsigned lSizeY = 1;
            for(unsigned s = 0; s < pSamples; ++s)
            {
                unsigned temp2 = 0;
                for(unsigned lY = 0; lY < lSizeY; ++lY)
                {
                    for(unsigned lX = 0; lX < lSizeX; ++lX)
                    {
                        float r1 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                        float r2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                        //r1 = r2 = 0;
                        ray.setMin(0.f);
                        pc.getRayPixel(x, y, 
                                (1.f / pSamples) * s, (1.f / pSamples) * s,
                                (1.f / lSizeX) * (lX + r1), (1.f / lSizeY) * (lY + r2), ray);
                        //ray.setMin(0.1f);
                        if(t.intersect(ray, intersection))
                        {
                            ++temp2;
                        }
                    }
                    //float r1 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    //float r2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                    
                }
                temp += temp2 / (float)(lSizeX * lSizeY);
            }
            //cBuff[0][y][x][0] = temp;
            unsigned char res = temp == 0.f ? 0.f : (256 / ( pSamples ) * temp - 1);
            cBuff[0][y][x][2] = cBuff[0][y][x][1] = 
            cBuff[0][y][x][0] = res;
        }
    }
    
    toPPM(900, 600, (unsigned *)buff);
    delete[] buff;
}

/*
 * 
 */
int main(int argc, char** argv)
{
    //Assumptions Check
    if(!checkRequirements()) 
    {
        cout << "This device cannot properly run this software" << endl;
        return -1;
    }
    
    //Assumptions Okay, Start Ray Tracer
    /* This is test code */
    using namespace Candela::Mathematics;
    Vector v (1.f, 2.f, 3.f);
    v = 3.f * v;
    BoundedRay r (v, v);
    cout << "Okay! " << v << endl << r << endl;
    cout << sizeof(BoundedRay);
    
    {
        using namespace Candela::Shape;
        cout << " " << sizeof(IBoundingShape);
        cout << "\nE: " << Candela::Mathematics::Constants::Pi << endl;
        Float::decompose(Constants::Pi);
    }
    
    cout << "\n Cylinder Test\n";
    BoundedRay br ( Vector(3.f,0.f,0.f), Vector(-2.f, 1.f, 0.f) );
    Candela::Shape::Intersection i;
    Candela::Shape::Cylinder cyl ( 0.f, Vector(0.f, 1.f, 0.f), 1.f);
    bool intersected = cyl.intersect(br, i);
    cout << intersected << ": " << i.Distance << endl;
    
    Candela::Shape::Triangle t ( Vector(1,1,0), Vector(2,1,0) , Vector(2,2,0));
    cout << t.getSurfaceArea() << endl;
    
    cout << (Vector::UnitZ ^ Vector::UnitX) << endl;
    
    cout << "Camera Test..." << endl;
    Candela::Camera::RealPinholeCamera rpc ( Vector::UnitZ ,
            Candela::Camera::CameraPlane( Rectangle_F (3.6f, 2.4f), 7.5f ) ,
            Rectangle_I(0, 900, 0, 600) ) ;
    //tlc.setFocalLength(5.f);
    //tlc.lookAt(Vector::UnitZ, Vector::UnitY + Vector::UnitX);
    
    rpc.getRayPixel(899, 600, 0.533f, 0.f, br);
    cout << rpc << endl;
    cout << br << endl;
    //testRender();
    TMatrix<float, 2, 3> matA;
    matA(0, 0) = 1;
    matA(0, 1) = 2;
    matA(0, 2) = 3;
    matA(1, 0) = 4;
    matA(1, 1) = 5;
    matA(1, 2) = 6;
    
    TMatrix<float, 3, 2> matB;
    matB(0, 0) = 7;
    matB(0, 1) = 8;
    matB(1, 0) = 9;
    matB(1, 1) = 10;
    matB(2, 0) = 11;
    matB(2, 1) = 12;
    
    TMatrix<float, 2, 2> result = matA * matB;
    std::cout << result(0, 0) << ", " << result(0, 1) << std::endl;
    std::cout << result(1, 0) << ", " << result(1, 1) << std::endl;
    //std::cout << sizeof(TMatrix<float, 3, 2>);
    /* End of test code */
    
    //std::cout << "Z at 440: " << Candela::Spectrum::Spectrum::Z(440.f) << std::endl;
    
    Algebra::DiscreteFunction df;
    df.addPoint(2, 4.5);
    df.addPoint(3, 5.5);
    df.addPoint(5, 12.5);
    df.addPoint(6, 45.5);
    df.addPoint(7, 5.5);
    df.addPoint(9, 0.5);
    std::cout << df(6.7f) << std::endl;
    return 0;
}

