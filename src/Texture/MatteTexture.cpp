/* 
 * File:   MatteTexture.cpp
 * Author: Kevin
 * 
 * Created on September 15, 2014, 10:06 PM
 */

#include "Texture/MatteTexture.h"

using namespace Candela::Texture;

MatteTexture::MatteTexture(const Spectrum::ISpectrum* a_spectrum) 
    : spectrum ( a_spectrum )
{

}

const Candela::Spectrum::ISpectrum* MatteTexture::GetSpectrum(const Mathematics::Vector2<float>& uv) const {
    return spectrum;
}
