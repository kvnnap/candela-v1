/* 
 * File:   LinearMesh.cpp
 * Author: Kevin
 * 
 * Created on 14 September 2014, 16:06
 */

#include "Mesh/LinearMesh.h"

using namespace Candela::Mesh;

float LinearMesh::getSurfaceArea() const {
    return 0.f;
}

bool LinearMesh::intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const {
    Shape::Intersection tempIntersection;
    Mathematics::BoundedRay tempRay ( ray );
    for(std::vector<const IShape*>::const_iterator it = meshList_.begin(); 
            it != meshList_.end(); 
            ++it)
    {
        const IShape * const shape ( *it );
        if(shape->intersect(tempRay, tempIntersection)){
            tempRay.setMax(tempIntersection.Distance);
        }
    }
    
    //check if any intersections
    if(tempIntersection.Type != Shape::Intersection::None)
    {
        intersection = tempIntersection;
        return true;
    }else{
        return false;
    }
}

void LinearMesh::add(const Shape::IShape& shape) {
    meshList_.push_back(&shape);
}

bool LinearMesh::remove(const Shape::IShape& shape) {
    if(meshList_.size() != 0)
    {
        for(unsigned i = 0; i < meshList_.size(); ++i)
        {
            if(meshList_[i] == &shape)
            {
                meshList_.erase(meshList_.begin() + i);
                return true;
            }
        }
        return false;
    }
    else
    {
        return false;
    }
}

void LinearMesh::clear() {
    meshList_.clear();
}
