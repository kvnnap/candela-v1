/* 
 * File:   RgbSpectrum.cpp
 * Author: Kevin
 * 
 * Created on 26 April 2014, 13:34
 */

#include "Mathematics/Matrix.h"
#include "Spectrum/XyzSpectrum.h"
#include "Spectrum/RgbSpectrum.h"
#include "Mathematics/Funtions.h"

using namespace Candela::Spectrum;

RgbSpectrum::RgbSpectrum()
{
}

RgbSpectrum::RgbSpectrum(float value) 
        : TriSpectrum( value )
{
}

RgbSpectrum::RgbSpectrum(const RgbSpectrum& rgb)
        : TriSpectrum(rgb)
{
}

RgbSpectrum::RgbSpectrum(const Mathematics::Vector& rgb)
        : TriSpectrum(rgb)
{
}

RgbSpectrum::RgbSpectrum(float red, float green, float blue)
        : TriSpectrum(red, green, blue)
{
}

float RgbSpectrum::getWaveLength(unsigned sampleIndex) const {
    return sampleIndex == 0 ? 700.f : (sampleIndex == 1 ? 546.1f : 435.8f);
}

float RgbSpectrum::luminance() const {
    return 0.2126729f * abc_.x + 0.7151522f * abc_.y + 0.0721750f * abc_.z;
}

RgbSpectrum RgbSpectrum::toTristimulusSRGB() const {
    return *this;
}

/*
 * use this matrix
 *      0.4124564  0.3575761  0.1804375
        0.2126729  0.7151522  0.0721750
        0.0193339  0.1191920  0.9503041
 * and multiply it by this->rgb_
 */
XyzSpectrum RgbSpectrum::toTristimulusXYZ() const
{
    using namespace Candela::Mathematics;
    Matrix_F m (3, 3);
    
    m(0, 0) = 0.4124564f;
    m(0, 1) = 0.3575761f;
    m(0, 2) = 0.1804375f;
    
    m(1, 0) = 0.2126729f;
    m(1, 1) = 0.7151522f;
    m(1, 2) = 0.0721750f;
    
    m(2, 0) = 0.0193339f;
    m(2, 1) = 0.1191920f;
    m(2, 2) = 0.9503041f;
    
    return XyzSpectrum(m * abc_);
}

//Need to edit clamp function to desaturate function

//from linear to screen non-linear (normal rgb)
void RgbSpectrum::alphaCorrect() 
{
    clamp();
    abc_.x = abc_.x <= 0.0031308f ? abc_.x * 12.92f : 1.055f * Mathematics::Functions::pow(abc_.x, 1.f / 2.4f) - 0.055f;
    abc_.y = abc_.y <= 0.0031308f ? abc_.y * 12.92f : 1.055f * Mathematics::Functions::pow(abc_.y, 1.f / 2.4f) - 0.055f;
    abc_.z = abc_.z <= 0.0031308f ? abc_.z * 12.92f : 1.055f * Mathematics::Functions::pow(abc_.z, 1.f / 2.4f) - 0.055f;
}

//from screen (normal rgb) to linear
void RgbSpectrum::inverseAlphaCorrect() 
{
    clamp();
    abc_.x = abc_.x <= 0.04045f ? abc_.x / 12.92f : Mathematics::Functions::pow((abc_.x + 0.055f) / 1.055f, 2.4f);
    abc_.y = abc_.y <= 0.04045f ? abc_.y / 12.92f : Mathematics::Functions::pow((abc_.y + 0.055f) / 1.055f, 2.4f);
    abc_.z = abc_.z <= 0.04045f ? abc_.z / 12.92f : Mathematics::Functions::pow((abc_.z + 0.055f) / 1.055f, 2.4f);
}