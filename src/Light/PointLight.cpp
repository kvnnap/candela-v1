/* 
 * File:   PointLight.cpp
 * Author: Kevin
 * 
 * Created on August 25, 2014, 8:53 PM
 */

#include "Light/PointLight.h"
#include "Mathematics/Constants.h"

using namespace Candela::Light;
using namespace Candela::Spectrum;

PointLight::PointLight(const Mathematics::Vector& point, Spectrum::ISpectrum* a_spectrum)
    : spectrum ( a_spectrum )
{
    //divide the power by a sphere total steradiance.
    (*spectrum) *= Candela::Mathematics::Constants::OneOverFourPi;
}

float PointLight::power() const {
    //calculate power in watts using spectrum
}

void PointLight::radiance(const Mathematics::Ray& ray, Spectrum::ISpectrum& a_spectrum) const {
    //wherever the ray is coming from, we have same radiance,
    //no cosine required because a point Light is an infinitesimally small Sphere
    Mathematics::Vector v = point - ray.getPosition();
    spectrum->divide(v * v, a_spectrum);
}
