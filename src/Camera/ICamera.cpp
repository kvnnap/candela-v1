/* 
 * File:   ICamera.cpp
 * Author: Kevin
 * 
 * Created on 19 March 2014, 11:06
 */

#include "Camera/ICamera.h"
#include <sstream>

using namespace Candela::Camera;

ICamera::ICamera(const Mathematics::Vector& direction, const Mathematics::Rectangle_F& rect, 
        float distance, Aperture apt)
        : filmPlane_ ( rect, distance), aperture_ ( apt ), apertureSize_ ( )
{
    lookAt(direction);
}

ICamera::ICamera(const Mathematics::Vector& direction, const CameraPlane& cp
                , Aperture apt)
        : filmPlane_ ( cp ), aperture_ ( apt ), apertureSize_ ( )
{
    lookAt(direction);
}


ICamera::~ICamera() {
}

void ICamera::setCameraPosition(const Mathematics::Vector& position) {
    position_ = position;
    changeListener();
}

void ICamera::setFilmPlane(const CameraPlane& cp) {
    filmPlane_ = cp;
    changeListener();
}

void ICamera::setFilmPlaneDistance(float distance) {
    filmPlane_.distance = distance;
    changeListener();
}

void ICamera::lookAt(const Mathematics::Vector& direction, const Mathematics::Vector& up) {
    //set orthonormal basis
    w_ = ~direction;
    u_ = ~(up ^ direction);
    v_ = ~(w_ ^ u_);
    changeListener();
}

void ICamera::setApertureSize(float apertureSize) {
    if(aperture_ != ICamera::Infinitesimal)
    {
        apertureSize_ = apertureSize;
        changeListener();
    }
}

void ICamera::changeListener() {

}

const CameraPlane& ICamera::getFilmPlane() const {
    return filmPlane_;
}

const ICamera::Aperture ICamera::getAperture() const {
    return aperture_;
}

float ICamera::getApertureSize() const {
    return apertureSize_;
}

const CameraPlane& ICamera::getObjectPlane() const {
    return filmPlane_;
}


void ICamera::getRay(float x, float y, Mathematics::BoundedRay& ray) const {
    getRay(x, y, 0.f, 0.f, 0.f, 0.f, ray);
}

void ICamera::getRay(float x, float y, float pixelVarX, float pixelVarY, Mathematics::BoundedRay& ray) const {
    getRay(x, y, pixelVarX, pixelVarY, 0.f, 0.f, ray);
}


std::string ICamera::toString() const {
    using namespace std;
    ostringstream sstr; //use own buffer to pre-allocate/reserve?
    sstr << "Camera - Address = " << this 
            << "\n\tOrigin: " << position_
            << "\n\tOrthonormal: " << u_ << "\n\t" << v_ << "\n\t" << w_
            << "\n\tFilm Plane: " << filmPlane_;
    return sstr.str();
}

std::ostream& Candela::Camera::operator <<(std::ostream& strm, const ICamera& camera)
{
    return strm << camera.toString();
}