/* 
 * File:   Triangle.cpp
 * Author: Kevin
 * 
 * Created on 02 March 2014, 18:00
 */

#include "Shape/Triangle.h"
#include <sstream>
#include <cmath>

using namespace Candela::Shape;

Triangle::Triangle(const Mathematics::Vector& O, const Mathematics::Vector& A, const Mathematics::Vector& B) 
{
    vertex_[0] = O;
    vertex_[1] = A;
    vertex_[2] = B;
}

bool Triangle::intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const
{
    using namespace Candela::Mathematics;
    
    //these below are cache-able
    const Vector Q1 = vertex_[1] - vertex_[0];
    const Vector Q2 = vertex_[2] - vertex_[0];
    const Vector faceUnitNormal = ~(Q2 ^ Q1);
    
    //compute denominator
    const float denominator = faceUnitNormal * ray.getDirection();
    if(denominator != 0.f) // make this check or else risk of division by zero..
    {
        float temp = - (faceUnitNormal * (ray.getPosition() - vertex_[0])) / denominator;
        //make sure intersection is not behind the ray
        if(temp >= ray.getMin())
        {
            //this is closer than previous intersection
            if(temp <= ray.getMax())
            {
                //check whether it lies in the triangle
                const Vector R = ray.getPoint(temp) - vertex_[0];
                
                //below 4 values are cache-able
                const float Q1Sq = Q1 * Q1;
                const float Q2Sq = Q2 * Q2;
                const float Q1Q2 = Q1 * Q2;
                union{
                    float determinant;
                    float oneOverDeterminant;
                };
                
                determinant = Q1Sq * Q2Sq - Q1Q2 * Q1Q2;
                
                if(determinant != 0.f)
                {
                    oneOverDeterminant = 1.f / determinant;
                    const float RQ1 = R * Q1, RQ2 = R * Q2;
                    const float u = ( Q2Sq * RQ1 - Q1Q2 * RQ2 ) * oneOverDeterminant;
                    const float v = ( Q1Sq * RQ2 - Q1Q2 * RQ1 ) * oneOverDeterminant;
                    if( (u >= 0.f) && (v >= 0.f) && ((u + v) <= 1.f) )
                    //it is in triangle!
                    {
                        intersection.Intersectant = this;
                        intersection.Distance = temp;
                        intersection.Type = denominator < 0.f ? 
                            Intersection::External : Intersection::Internal;
                        return true;
                    }
                }
            }
        }
    }
    //no intersection
    return false;
}

float Triangle::getSurfaceArea() const {
    using namespace Candela::Mathematics;
    
    //these below are cache-able
    const Vector Q1 = vertex_[1] - vertex_[0];
    const Vector Q2 = vertex_[2] - vertex_[0];
    const float Q1Q2 = Q1 * Q2;
    
    return 0.5f * !Q1 * !Q2 * sqrt(1 - (Q1Q2 * Q1Q2 / (( Q1 * Q1 ) * (Q2 * Q2))));
}


std::string Triangle::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Triangle Shape - Address = " << this
         << "\n\tVertex[0]: " << vertex_[0]  << "\n\tVertex[1]:" << vertex_[1] 
         << "\n\tVertex[2]: " << vertex_[2] ;
    return sstr.str();
}