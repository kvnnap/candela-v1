/* 
 * File:   Plane.cpp
 * Author: Kevin
 * 
 * Created on 15 March 2014, 21:02
 */

#include "Shape/Plane.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Shape;

Plane::Plane(const Mathematics::Vector& position, const Mathematics::Vector& normal)
        : position_ ( position ), unitNormal_ ( ~normal )
{
}

bool Plane::intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const {
    //compute denominator
    const float den = unitNormal_ * ray.getDirection();
    if(den != 0.f) // make this check or else risk of division by zero..
    {
        float temp = - (unitNormal_ * (ray.getPosition() - position_)) / den;
        //make sure intersection is not behind the ray
        if(temp >= ray.getMin())
        {
            if(temp <= ray.getMax())
            {
                intersection.Intersectant = this;
                intersection.Distance = temp;
                intersection.Type = den < 0.f ? 
                            Intersection::External : Intersection::Internal;
                return true;
            }
        }
    }
    return false;
}


float Plane::getSurfaceArea() const {   
    return Mathematics::Float::Infinity;
}


std::string Plane::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Plane Shape - Address = " << this
         << "\n\tPosition: " << position_  << "\n\tUnitNormal: " << unitNormal_; 
    return sstr.str();
}

