/* 
 * File:   Cylinder.cpp
 * Author: Kevin
 * 
 * Created on 30 December 2013, 15:37
 */

#include "Shape/Cylinder.h"
#include "Mathematics/Constants.h"
#include <sstream>
#include <math.h>

using namespace Candela::Shape;

Cylinder::Cylinder(const Mathematics::Vector& centre,
        const Mathematics::Vector& direction, float radius)
        : centre_ ( centre ), direction_ ( direction ), radius_ ( radius )
{
}

bool Cylinder::intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const 
{
    using namespace Candela::Mathematics;
    
    /*
     * centre_ = S, direction_ = Q, O = ray position and D = ray direction
     */
    
    const Vector& D = ray.getDirection();
    
    const Vector OminusS = ray.getPosition() - centre_;
    const float OneOverQSquared = 1.f / (direction_ * direction_); /*1 / (Q dot Q)*/
    const float OminusSdotQ = OminusS * direction_;
    const float DdotQ = D * direction_;
    
    union{
        float temp;
        float determinant;
        float OneOver2A;
    };
    
    const float a = D * D - DdotQ * (temp = DdotQ * OneOverQSquared);
    const float b = 2.f * (OminusS * D - OminusSdotQ * temp);
    ;
    //determinant
    determinant = b * b - 4.f * a * (OminusS * OminusS - 
                OminusSdotQ * OminusSdotQ * OneOverQSquared - radius_ * radius_);
    
    if(determinant >= 0.f) /* check this float comparison out, handle exact zero separately*/
    {
        const float rootDet = sqrt(determinant);
        OneOver2A = 1.f / (2.f * a);
        float tMin = (-b - rootDet) * OneOver2A;
        
        //check if given max bound is smaller than cylinder's min, if it is return!
        if(ray.getMax() < tMin)
        {
            return false;
        }
        
        float tMax = (- b + rootDet) * OneOver2A;
        //check if object is behind the ray
        if(tMax < ray.getMin())
        {
            return false;
        }
        //inside cylinder - only one possible intersection - tMax
        if(tMin < ray.getMin())
        {
            /* Check that k is in bounds and that ray can reach  internal surface */
            const float k = (OminusSdotQ + tMax * DdotQ) * OneOverQSquared;
            if( (k < 0.f) || (k > 1.f) || ray.getMax() < tMax )
            {
                return false;
            }else
            {
                intersection.Distance = tMax;
                intersection.Type = Intersection::Internal;
            }
        }//two intersections - get closest
        else
        {
            float k = (OminusSdotQ + tMin * DdotQ) * OneOverQSquared;
            if( (k >= 0.f) && (k <= 1.f) )
            {
                intersection.Distance = tMin;
                intersection.Type = Intersection::External;
            }else
            {
                //closest was not in bounds... try tMax
                k = (OminusSdotQ + tMax * DdotQ) * OneOverQSquared;
                if( (k < 0.f) || (k > 1.f) || ray.getMax() < tMax )
                {
                    return false;
                }else
                {
                    intersection.Distance = tMax;
                    intersection.Type = Intersection::Internal;
                }
            }
            
        }
        intersection.Intersectant = this;
        return true;
    }else
    {
        return false;
    }
}


float Cylinder::getSurfaceArea() const
{
    return 2 * Mathematics::Constants::Pi * radius_ /* * k, but is 1*/;
}

float Cylinder::getVolume() const
{
    return Mathematics::Constants::Pi * radius_ * radius_ /* * k, but is 1*/;
}

void Cylinder::contain(const IShape& shape)
{
}

void Cylinder::contain(const std::vector<const IShape*>& shapes)
{
}

std::string Cylinder::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Cylinder Shape - Address = " << this
         << "\n\tCenter: " << centre_  << "\n\tDirection:" << direction_ 
         << "\n\tRadius: " << radius_ ;
    return sstr.str();
}
