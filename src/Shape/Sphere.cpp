/* 
 * File:   Sphere.cpp
 * Author: Kevin
 * 
 * Created on 26 December 2013, 17:46
 */

#include "Shape/Sphere.h"
#include "Mathematics/Constants.h"

#include <sstream>
#include <math.h>

using namespace Candela::Shape;

Sphere::Sphere(const Mathematics::Vector& centre, float radius)
        : centre_ ( centre ), radius_ ( radius ), radiusSquared_ ( radius * radius )
{

}

bool Sphere::intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const 
{
    using namespace Candela::Mathematics;
    
    //get vector from sphere centre to ray origin
    const Vector pminusc = ray.getPosition() - centre_;
    const Vector& rayDir = ray.getDirection();
    //error might accumulate below :/
    const float A = rayDir * rayDir;
    const float B = 2.f * (pminusc * rayDir);
    
    //determinant united with denominator for memory efficiency
    //remember to always read and write from same variable in union in standard C++
    //G++ allows read and write from different variable though..
    union { 
        float determinant;
        float denominator;
    };
    
    //determinant
    determinant = B * B - 4.f * A * (pminusc * pminusc - radiusSquared_);
    
    //det negative = miss
    if(determinant >= 0.f)
    {
        const float st = sqrt(determinant);
        
        denominator = 1.f / (2.f * A);
        const float tMin = (- B - st) * denominator;
        
        //check if given max bound is smaller than sphere's min, if it is return!
        if(ray.getMax() < tMin)
        {
            return false;
        }
        
        const float tMax = (- B + st) * denominator;
        //check if object is behind the ray - hence if direction is negative..
        if(tMax < ray.getMin())
        {
            return false;
        }
        //inside sphere
        if(tMin < ray.getMin())
        {
            //check if ray reaches for surface internally
            if(ray.getMax() < tMax)
            {
                return false;
            }else
            {
                intersection.Distance = tMax;
                intersection.Type = Intersection::Internal;
            }
        }//two intersections - get closest
        else
        {
            
            intersection.Distance = tMin;
            intersection.Type = Intersection::External;
        }
        intersection.Intersectant = this;
        return true;
    }else
    {
        return false;
    }
}


float Sphere::getSurfaceArea() const 
{
    //need pi
    return 4.f * Candela::Mathematics::Constants::Pi * radiusSquared_;
}

float Sphere::getVolume() const 
{
    return (4.f / 3.f) * Candela::Mathematics::Constants::Pi * radiusSquared_ * radius_;
}

void Sphere::contain(const IShape& shape)
{
}

void Sphere::contain(const std::vector<const IShape*>& shapes)
{
}

std::string Sphere::toString() const {
    using namespace std;
    ostringstream sstr;
    sstr << "Sphere Shape - Address = " << this
         << "\n\tCenter: " << centre_  << "\n\tRadius:" << radius_ ;
    return sstr.str();
}


