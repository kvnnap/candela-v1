/* 
 * File:   AxisAlignedBoundingBox.cpp
 * Author: Kevin
 * 
 * Created on 15 March 2014, 22:38
 */

#include "Shape/AxisAlignedBoundingBox.h"
#include "Mathematics/Float.h"
#include <sstream>

using namespace Candela::Shape;

AxisAlignedBoundingBox::AxisAlignedBoundingBox() 
{
}

AxisAlignedBoundingBox::AxisAlignedBoundingBox(Mathematics::Vector minPosition, 
        Mathematics::Vector maxPosition) 
        : minPosition_ ( minPosition ), maxPosition_ ( maxPosition_ )
{

}

AxisAlignedBoundingBox::AxisAlignedBoundingBox(Mathematics::Vector centre, 
        float radius) 
        : minPosition_ ( centre - radius ), 
        maxPosition_ ( centre + radius )
{

}


bool AxisAlignedBoundingBox::intersect(const Mathematics::BoundedRay& ray, 
        Intersection& intersection) const 
{
    using namespace Candela::Mathematics;
    //right left - x normal
    const Vector& rPos = ray.getPosition();
    const Vector& rDir = ray.getDirection();
    const Vector& rMin = ray.getMinPoint();
    float tTemp;
    
    if(rMin.x > minPosition_.x && rMin.x <= maxPosition_.x && 
       rMin.y > minPosition_.y && rMin.y <= maxPosition_.y && 
       rMin.z > minPosition_.z && rMin.z <= maxPosition_.z)
    {
        //INSIDE BOX
        float t = Float::Infinity;
        //find the maximum t-Value allowed in order for ray to stay in box
        for(int i = 0; i < 3; i++)
        {
            if(rDir.xyz[i] != 0.f)
            {
                tTemp = rDir.xyz[i] < 0.f ? (minPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i] 
                                   : (maxPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i];
                if(tTemp < t)
                {
                    t = tTemp;
                }
            }
        }
        if(ray.getMax() < t)
        {
            return false;
        }else{
            intersection.Type = Intersection::Internal;
            intersection.Distance = t;
            intersection.Intersectant = this;
            return true;
        }
    }else
    {
        //loop through faces
        for(int i = 0; i < 3; i++)
        {
            //from outside
            if(rDir.xyz[i] < 0.f)
            {//check right face or up or inward
                tTemp = (maxPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i];
            }else if(rDir.xyz[i] > 0.f)
            {//check left face or down or outward
                tTemp = (minPosition_.xyz[i] - rPos.xyz[i]) / rDir.xyz[i];
            }else
            {//cant perform division by zero
                continue;
            }


            if(tTemp >= ray.getMin() && tTemp <= ray.getMax())
            {
                const int sec = (i + 1) % 3;
                const int third = (i + 2) % 3;
                //create intersection point
                const Vector& rXYZ = ray.getPoint(tTemp);
                //check whether it is contained
                if(rXYZ.xyz[sec] >= minPosition_.xyz[sec] && rXYZ.xyz[sec] <= maxPosition_.xyz[sec] && 
                   rXYZ.xyz[third] >= minPosition_.xyz[third] && rXYZ.xyz[third] <= maxPosition_.xyz[third])
                {
                    //intersected - no other intersections will occur!
                    intersection.Type = Intersection::External;
                    intersection.Distance = tTemp;
                    intersection.Intersectant = this;
                    return true;

                }
            }
        }
    }
    return false;
}


float AxisAlignedBoundingBox::getSurfaceArea() const 
{   
    const Mathematics::Vector size = maxPosition_ - minPosition_;
    return 2 * (size.x * (size.y + size.z) + size.y * size.z);
}


std::string AxisAlignedBoundingBox::toString() const 
{
    using namespace std;
    ostringstream sstr;
    sstr << "Plane Shape - Address = " << this
         << "\n\tMinPosition: " << minPosition_  << "\n\tMaxPosition: " << maxPosition_; 
    return sstr.str();
}

float AxisAlignedBoundingBox::getVolume() const {
    const Mathematics::Vector size = maxPosition_ - minPosition_;
    return size.x * size.y * size.z;
}

void AxisAlignedBoundingBox::contain(const IShape& shape) {

}

void AxisAlignedBoundingBox::contain(const std::vector<const IShape*>& shapes) {

}