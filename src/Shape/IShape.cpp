/* 
 * File:   IShape.cpp
 * Author: Kevin
 * 
 * Created on 22 December 2013, 21:39
 */

#include "Shape/IShape.h"

#include <sstream>

using namespace Candela::Shape;

IShape::~IShape() {}

std::string IShape::toString() const
{
    using namespace std;
    ostringstream sstr;
    sstr << "IShape - Address = " << this;
    return sstr.str();
}

std::ostream& Candela::Shape::operator <<(std::ostream& strm, const IShape& ishape)
{
    return strm << ishape.toString();
}