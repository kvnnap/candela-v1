/* 
 * File:   Float.cpp
 * Author: Kevin
 * 
 * Created on 18 December 2013, 17:24
 */

#include <iostream>
#include <cstdlib>
#include <limits>
#include <stdexcept>
#include "Mathematics/Float.h"

using namespace Candela::Mathematics;

void Float::decompose(float num)
{
    using namespace std;
    
    if(!IEEE754 || sizeof(float) != sizeof(int))
    {
        //different size or incompatible
        throw runtime_error("Float is different size or not running IEEE754");
    }
    
    //g++ allows type-punning using unions - not standard!
    union{
        float a;
        unsigned int b;
    };
    a = num;
    //should become a logical shift when used with unsigned
    unsigned int sign = ((unsigned int)(b & 0x80000000)) >> 31;
    unsigned int exponent = ((unsigned int)(b & 0x7F800000)) >> 23;
    unsigned int mantissa = (unsigned int)(b & 0x007FFFFF);
    
    //could use boost::format
    cout << a << " = Sign: " << sign << ", Exponent: " << exponent 
            << " - 127 = " << (int)(exponent-127) << ", Mantissa: " << mantissa << endl;
}

bool Float::almostEqualUlps(float f, float s, unsigned int maxUlps)
{
    //check pure equality - also -0.0 == 0.0 works here
    if (f == s)
    {
       return true; 
    }
    
    //if maxUlps is zero, then only PURE Equality is expected, thus not equal.
    if(maxUlps == 0) 
    {
        return false;
    }
    
    if(!IEEE754 || sizeof(float) != sizeof(int))
    {
        //different size or incompatible
        throw std::runtime_error("Float is of different size or not running IEEE754");
    }
    
    //define union
    union U {
        float a;
        int b;
    } A, B;
    
    //apply
    A.a = f;
    B.a = s;
    
    //re-map to restore lexicographic order
    if(A.b < 0)
        A.b = 0x80000000 - A.b;
    if(B.b < 0)
        B.b = 0x80000000 - B.b;
    
    //done
    return abs(A.b - B.b) <= maxUlps;
}

const bool  Float::IEEE754 = std::numeric_limits<float>::is_iec559;
const float Float::Infinity = std::numeric_limits<float>::infinity();
const float Float::NegativeInfinity = -std::numeric_limits<float>::infinity();
const float Float::Maximum = std::numeric_limits<float>::max();
const float Float::Minimum = -std::numeric_limits<float>::max();
const float Float::MachineEpsilon = std::numeric_limits<float>::epsilon();