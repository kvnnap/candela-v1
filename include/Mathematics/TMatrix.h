/* 
 * File:   TMatrix.h
 * Author: Kevin
 *
 * Created on 19 April 2014, 13:30
 */

#ifndef TMATRIX_H
#define	TMATRIX_H

#include "Vector3.h"

namespace Candela
{
    namespace Mathematics
    {
        
        template<bool B, class T, class F>
        struct conditional { typedef T type; };
        template<class T, class F>
        struct conditional<false, T, F> { typedef F type; };
        
        template<class T, unsigned R, unsigned C>
        class TMatrix;
        
        template<class T, unsigned R, unsigned C>
        TMatrix<T, R, C> operator * (T scalar, const TMatrix<T, R, C>& matrix);
        
        template<class T, unsigned R, unsigned C>
        class TMatrix {
        public:
            TMatrix();
            
            //getters - DEFINE THESE AS CONSTEXPR in C++11!!!!!!!! Cos these are compile time constant functions
            unsigned char getRows() const;
            unsigned char getColumns() const;
            bool isSquare() const;
            TMatrix getIdentity() const;
            //static getters
            static TMatrix<T, 4, 4> getRotationX(T theta);
            static TMatrix<T, 4, 4> getRotationY(float theta);
            static TMatrix<T, 4, 4> getRotationZ(float theta);
            static TMatrix<T, 4, 4> getScaling(const Vector& scale);
            static TMatrix<T, 4, 4> getTranslation(const Vector& translation);

            //math operators - conditional<sizeof(T) >= sizeof(T2), T, T2>::type
            template<unsigned R2, unsigned C2, class T2>
            TMatrix<typename conditional<sizeof(T) >= sizeof(T2), T, T2>::type, R, C2> operator * (const TMatrix<T2, R2, C2>& matrix) const;
            //accepts only 3 or 4 columns as the 'this' matrix
            Vector operator * (const Vector& vector) const;
            TMatrix operator * (float scalar) const;
            //this is global in this namespace
            
            template<class T2, unsigned R2, unsigned C2>
            friend TMatrix<T2, R2, C2> operator * (T2 scalar, const TMatrix<T2, R2, C2>& matrix);
            
            TMatrix operator + (const TMatrix& matrix) const;
            TMatrix operator - (const TMatrix& matrix) const;

            template<unsigned R2, unsigned C2>
            const TMatrix& operator *= (const TMatrix<T, R2, C2>& matrix);
            const TMatrix& operator += (const TMatrix& matrix);
            const TMatrix& operator -= (const TMatrix& matrix);
            //http://my.safaribooksonline.com/book/programming/cplusplus/0201309831/operator-overloading/ch23lev1sec5
            T& operator() (unsigned row, unsigned column);
            T operator() (unsigned row, unsigned column) const;
        private:
            T data_[R][C];
        };
    }
}

#include "../../src/Mathematics/TMatrix.inl"

#endif	/* TMATRIX_H */

