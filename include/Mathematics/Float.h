/* 
 * File:   Float.h
 * Author: Kevin
 *
 * Created on 18 December 2013, 17:24
 */

#ifndef FLOAT_H
#define	FLOAT_H

namespace Candela
{
    namespace Mathematics
    {
        class Float {
        public:
            static void decompose(float num);
            static bool almostEqualUlps(float f, float s, unsigned int maxUlps);
            
            static const float Maximum;
            static const float Minimum;
            static const float Infinity;
            static const float NegativeInfinity;
            static const float MachineEpsilon;
            static const bool IEEE754;
        private:
            
        };
    }
}



#endif	/* FLOAT_H */

