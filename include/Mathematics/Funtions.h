/* 
 * File:   Funtions.h
 * Author: Kevin
 *
 * Created on 19 April 2014, 18:57
 */

#ifndef FUNTIONS_H
#define	FUNTIONS_H

namespace Candela
{
    namespace Mathematics
    {
        //instead of static class
        //define functions in header later and compare speedup
        namespace Functions
        {
            float sin(float theta);
            double sin(double theta);
            float cos(float theta);
            double cos(double theta);
            float pow(float num, float exp);
            double pow(double num, double exp);
            float ln(float num);
            double ln(double ln);
            
            float add(float, float);
            float subtract(float, float);
            float multiply(float, float);
            float divide(float, float);
            double add(double, double);
            double subtract(double, double);
            double multiply(double, double);
            double divide(double, double);
            
        }
    }
            
}

#endif	/* FUNTIONS_H */

