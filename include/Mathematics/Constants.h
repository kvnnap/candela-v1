/* 
 * File:   Constants.h
 * Author: Kevin
 *
 * Created on 28 December 2013, 14:07
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H


namespace Candela
{
    namespace Mathematics
    {

        class Constants {
        public:
            static const float Pi;
            static const float PiOver2;
            static const float PiOver4;
            static const float OneOverPi;
            static const float TwoOverPi;
            static const float E;
            static const float Ln2;
            static const float Ln10;
            static const float OneOverLn2;
            static const float OneOverLn10;
            static const float FourPi;
            static const float OneOverFourPi;
            
        private:

        };
    }
}

#endif	/* CONSTANTS_H */

