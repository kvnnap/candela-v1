/* 
 * File:   IFunction.h
 * Author: Kevin
 *
 * Created on 15 June 2014, 14:05
 */

#ifndef IFUNCTION_H
#define	IFUNCTION_H

#include <sstream> 

namespace Candela
{
    namespace Mathematics
    {
        namespace Algebra
        {
            template<class T>
            class IFunction {
            public:
                virtual ~IFunction();
                virtual T operator() (T x) const = 0;
                virtual std::string toString() const = 0;

            private:

            };
            
            template<class T>
            std::ostream& operator<< (std::ostream&, const IFunction<T>&);
        }
    }
}

#include "../../../src/Mathematics/Algebra/IFunction.inl"

#endif	/* IFUNCTION_H */

