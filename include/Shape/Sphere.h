/* 
 * File:   Sphere.h
 * Author: Kevin
 *
 * Created on 26 December 2013, 17:46
 */

#ifndef SPHERE_H
#define	SPHERE_H

#include "IBoundingShape.h"
#include "Mathematics/BoundedRay.h"

namespace Candela
{
    namespace Shape
    {
        class Sphere
                : public IBoundingShape
        {
        public:
            Sphere(const Mathematics::Vector& centre, float radius);
            
            /* Inherited from IShape */
            virtual std::string toString() const;
            virtual bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const;
            virtual float getSurfaceArea() const;
            
            /* Inherited from IBoundingShape */
            virtual float getVolume() const;
            virtual void contain(const IShape& shape);
            virtual void contain(const std::vector<const IShape*>& shapes);
            
        private:
            Mathematics::Vector centre_;
            float radius_;
            
            //cache
            float radiusSquared_;
        };
    }
}

#endif	/* SPHERE_H */

