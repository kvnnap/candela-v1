/* 
 * File:   Intersection.h
 * Author: Kevin
 *
 * Created on 23 December 2013, 22:22
 */

#ifndef INTERSECTION_H
#define	INTERSECTION_H

namespace Candela
{
    namespace Shape
    {
        //Forward Declaration
        class IShape;
        
        /*defines a Shape Intersection storing information of a particular intersection*/
        class Intersection 
        {
        public:
            
            
            enum EType{
                None = 0, //No Intersection
                External = 1, //External Intersection ( Normal of surface Pointing Out)
                Internal = 2 //Internal Intersection ( Normal of surface Pointing In)
            };
            
            Intersection() : Intersectant(NULL), Distance ( 0.f ), Type ( None ) {}

            
            //The shape which intersected with ray
            const IShape * Intersectant;
            //distance from origin of ray
            float Distance;
            //is it an internal intersection? such as inside of a sphere or back of triangle
            EType Type;
        };
    }
}

#endif	/* INTERSECTION_H */

