/* 
 * File:   IBoundingShape.h
 * Author: Kevin
 *
 * Created on 26 December 2013, 12:28
 */

#ifndef IBOUNDINGSHAPE_H
#define	IBOUNDINGSHAPE_H

#include "IShape.h"
#include <vector>

namespace Candela 
{
    namespace Shape
    {
        class IBoundingShape 
                : public IShape
        {
        public:
            //inherited virtual methods - USE Override in C++11 please 
            //and remove virtual keyword
            virtual std::string toString() const;
            
            /* New Virtual Methods*/
            virtual float getVolume() const = 0;
            
            virtual void contain(const IShape& shape) = 0;
            //expand this shape to contain all shapes in list
            virtual void contain(const std::vector<const IShape*>& shapes) = 0;
            /* End of New Virtual Methods */
            
        private:

        };
    }
}



#endif	/* IBOUNDINGSHAPE_H */

