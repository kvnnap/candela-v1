/* 
 * File:   IShape.h
 * Author: Kevin
 *
 * Created on 22 December 2013, 21:39
 */

#ifndef ISHAPE_H
#define	ISHAPE_H

#include <string>
#include "Mathematics/BoundedRay.h"
#include "Intersection.h"

namespace Candela
{
    namespace Shape
    {
        class IShape {
        public:
            virtual ~IShape();
            virtual std::string toString() const;
            
            //Intersection won't change this Shape; Use other method for MailBoxing!
            /*intersection object should not be touched if no intersection occurs*/
            virtual bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const = 0;
            
            //use ShapeBoundaryVisitor? - only to make extensible by others though:
            //virtual void accept(IBoundingShape& boundingShape) const = 0;
            //virtual IBoundingShape getBoundingVolume
            
            //other
            virtual float getSurfaceArea() const = 0;
            
        };
        
        std::ostream& operator<< (std::ostream& strm, const IShape& ishape);
    }
}

#endif	/* ISHAPE_H */

