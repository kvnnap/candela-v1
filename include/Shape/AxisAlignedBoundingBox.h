/* 
 * File:   AxisAlignedBoundingBox.h
 * Author: Kevin
 *
 * Created on 15 March 2014, 22:38
 */

#ifndef AXISALIGNEDBOUNDINGBOX_H
#define	AXISALIGNEDBOUNDINGBOX_H

#include "IBoundingShape.h"

namespace Candela
{
    namespace Shape
    {
        class AxisAlignedBoundingBox 
                : public IBoundingShape
        {
        public:
            AxisAlignedBoundingBox();
            AxisAlignedBoundingBox(Mathematics::Vector minPosition,
                                        Mathematics::Vector maxPosition);
            AxisAlignedBoundingBox(Mathematics::Vector centre,
                                        float radius);
            
            /* Inherited from IShape */
            virtual std::string toString() const;
            virtual bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const;
            virtual float getSurfaceArea() const;
            
            /* Inherited from IBoundingShape */
            virtual float getVolume() const;
            virtual void contain(const IShape& shape);
            virtual void contain(const std::vector<const IShape*>& shapes);
            
        private:
            Mathematics::Vector minPosition_, maxPosition_;
        };
    }
}

#endif	/* AXISALIGNEDBOUNDINGBOX_H */

