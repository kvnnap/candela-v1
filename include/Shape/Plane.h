/* 
 * File:   Plane.h
 * Author: Kevin
 *
 * Created on 15 March 2014, 21:02
 */

#ifndef PLANE_H
#define	PLANE_H

#include "IShape.h"
#include "Mathematics/Vector3.h"

namespace Candela
{
    namespace Shape
    {
        class Plane 
                : public IShape
        {
        public:
            Plane(const Mathematics::Vector& position, const Mathematics::Vector& normal);
            
            /* Inherited from IShape */
            virtual std::string toString() const;
            virtual bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const;
            virtual float getSurfaceArea() const;
            
        private:
            Mathematics::Vector position_, unitNormal_;
        };
    }
}

#endif	/* PLANE_H */

