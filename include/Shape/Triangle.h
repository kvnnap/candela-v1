/* 
 * File:   Triangle.h
 * Author: Kevin
 *
 * Created on 02 March 2014, 18:00
 */

#ifndef TRIANGLE_H
#define	TRIANGLE_H

#include "IShape.h"
#include "Mathematics/Vector3.h"

namespace Candela 
{
    namespace Shape 
    {
        class Triangle 
                : public IShape
        {
        public:
            Triangle(const Mathematics::Vector& O, const Mathematics::Vector& A, const Mathematics::Vector& B);
            
            /* Inherited from IShape */
            virtual std::string toString() const;
            virtual bool intersect(const Mathematics::BoundedRay& ray, Intersection& intersection) const;
            virtual float getSurfaceArea() const;
            
        private:
            Mathematics::Vector vertex_[3];
        };
    }
}



#endif	/* TRIANGLE_H */