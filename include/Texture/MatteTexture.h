/* 
 * File:   MatteTexture.h
 * Author: Kevin
 *
 * Created on September 15, 2014, 10:06 PM
 */

#ifndef MATTETEXTURE_H
#define	MATTETEXTURE_H

#include "ITexture.h"

namespace Candela
{
    namespace Texture
    {
        class MatteTexture 
            : public ITexture
        {
        public:
            MatteTexture(const Spectrum::ISpectrum * a_spectrum);
            
            const Spectrum::ISpectrum * GetSpectrum(const Mathematics::Vector2<float>& uv) const;
        private:
            
            const Spectrum::ISpectrum * spectrum;
        };

    }
}

#endif	/* MATTETEXTURE_H */

