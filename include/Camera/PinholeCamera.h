/* 
 * File:   PinholeCamera.h
 * Author: Kevin
 *
 * Created on 25 March 2014, 21:12
 */

#ifndef PINHOLECAMERA_H
#define	PINHOLECAMERA_H

#include "IMappedCamera.h"

namespace Candela
{
    namespace Camera
    {
        class PinholeCamera 
                : public IMappedCamera
        {
        public:
            PinholeCamera(const Mathematics::Vector& direction, 
                    const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect);
            
            virtual void changeListener();
            
            virtual void getRay(float x, float y, Mathematics::BoundedRay& ray) const;
            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY, Mathematics::BoundedRay& ray) const;
            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY, 
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const;
            
            virtual std::string toString() const;
        protected:
            PinholeCamera(const Mathematics::Vector& direction, 
                    const CameraPlane& cp, const Mathematics::Rectangle_I& pixelRect, 
                    ICamera::Aperture aperture);
        protected:
            Mathematics::Vector objectPlaneAperture_;
        };
    }
}



#endif	/* PINHOLECAMERA_H */

