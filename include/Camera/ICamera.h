/* 
 * File:   ICamera.h
 * Author: Kevin
 *
 * Created on 19 March 2014, 11:06
 */

#ifndef ICAMERA_H
#define	ICAMERA_H

#include "Mathematics/Vector3.h"
#include "Mathematics/BoundedRay.h"

#include "CameraPlane.h"

namespace Candela
{
    namespace Camera
    {
        class ICamera {
        public:
            
            enum Aperture{
                Infinitesimal = 0, //Ideal
                Circular = 1,
                Rectangular = 2 //This will mostly be used
            };
            
            //Constructors
            ICamera(const Mathematics::Vector& direction, const Mathematics::Rectangle_F& rect, 
                    float distance, Aperture apt);
            ICamera(const Mathematics::Vector& direction, const CameraPlane& cp,
                        Aperture apt);
            virtual ~ICamera();
            
            //Describes this camera
            virtual std::string toString() const;
            
            //Set Camera Position where centre of mass is the centre of aperture
            virtual void setCameraPosition(const Mathematics::Vector& position);
            virtual void setFilmPlane(const CameraPlane& cp);
            virtual void setFilmPlaneDistance(float distance);
            void lookAt(const Mathematics::Vector& direction,
                        const Mathematics::Vector& up = Mathematics::Vector::UnitY);
            virtual void setApertureSize(float apertureSize);
            virtual void changeListener();
            
            virtual const CameraPlane& getObjectPlane() const;
            const CameraPlane& getFilmPlane() const;
            const Aperture getAperture() const;
            float getApertureSize() const;
            
            /**
             * No sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x,y) point of the film plane
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param ray - The ray to be traced
             */
            virtual void getRay(float x, float y, Mathematics::BoundedRay& ray) const;
            
            /**
             * Pixel sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A small change in x to vary from original x position
             * @param pixelVarY - - A small change in y to vary from original x position
             * @param ray - The ray to be traced
             */
            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY, 
                                Mathematics::BoundedRay& ray) const;
            
            /**
             * Pixel and aperture sampling are assumed. The ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane, which will
             * also pass from (centreX + apertureVarX, centreY + apertureVarY) of aperture
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A small change in x to vary from original x position
             * @param pixelVarY - A small change in y to vary from original y position
             * @param apertureVarX - A small change in x to vary from original centre x position of aperture
             * @param apertureVarY - A small change in y to vary from original centre y position of aperture
             * @param ray
             */
            virtual void getRay(float x, float y, float pixelVarX, float pixelVarY, 
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const = 0;
            
        protected:
            //camera position
            Mathematics::Vector position_;
            //Orthonormal set
            Mathematics::Vector u_, v_, w_;
            //Film Plane definition relative to centre of Camera
            //should store in two Vector2, when I do one
            CameraPlane filmPlane_; 
            const Aperture aperture_;
            float apertureSize_;
        };
        
        std::ostream& operator<< (std::ostream& strm, const ICamera& camera);
    }
}

#endif	/* ICAMERA_H */

