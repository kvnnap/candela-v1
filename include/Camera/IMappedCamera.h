/* 
 * File:   IMappedCamera.h
 * Author: Kevin
 *
 * Created on 29 March 2014, 11:35
 */

#ifndef IMAPPEDCAMERA_H
#define	IMAPPEDCAMERA_H

#include "ICamera.h"
#include "Mathematics/Rectangle.h"

namespace Candela
{
    namespace Camera
    {

        class IMappedCamera 
                : public ICamera
        {
        public:
            IMappedCamera(const Mathematics::Vector& direction, const CameraPlane& cp,
                          Aperture aperture, const Mathematics::Rectangle_I& pixelRect);
            
            
            virtual void setPixelRect(const Mathematics::Rectangle_I& pixelRect);
            
            /*overrrides*/
            virtual void setFilmPlane(const CameraPlane& cp);
            virtual std::string toString() const;
            
            /**
             * No sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x,y) point of the pixel plane
             * @param x - x coordinate on the pixel plane
             * @param y - y coordinate on the pixel plane
             * @param ray - The ray to be traced
             */
            void getRayPixel(int x, int y, Mathematics::BoundedRay& ray) const;
            
            /**
             * Pixel sampling is assumed, the ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A variation of x bounded from 0 to 1
             * @param pixelVarY - A variation of y bounded from 0 to 1
             * @param ray - The ray to be traced
             */
            void getRayPixel(int x, int y, float pixelVarX, float pixelVarY, 
                                Mathematics::BoundedRay& ray) const;
            
            /**
             * Pixel and aperture sampling are assumed. The ray will trace the scene to finally recover
             * the colours in the (x + pixelVarX,y + pixelVarY) point of the film plane, which will
             * also pass from (centreX + apertureVarX, centreY + apertureVarY) of aperture
             * @param x - x coordinate on the film plane
             * @param y - y coordinate on the film plane
             * @param pixelVarX - A variation of x bounded from 0 to 1
             * @param pixelVarY - A variation of y bounded from 0 to 1
             * @param apertureVarX - A variation of x bounded from 0 to 1
             * @param apertureVarY - A variation of y bounded from 0 to 1
             * @param ray - The ray to be traced
             */
            void getRayPixel(int x, int y, float pixelVarX, float pixelVarY, 
                                float apertureVarX, float apertureVarY, Mathematics::BoundedRay& ray) const;
            
        private:
            
            void setRatio();
            
            Mathematics::Rectangle_I pixelRect_;
            float filmToPixelRatioX_, filmToPixelRatioY_;
        };
    }
}

#endif	/* IMAPPEDCAMERA_H */

