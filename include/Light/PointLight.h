/* 
 * File:   PointLight.h
 * Author: Kevin
 *
 * Created on August 25, 2014, 8:53 PM
 */

#ifndef POINTLIGHT_H
#define	POINTLIGHT_H

#include "ILight.h"
#include "Spectrum/Spectrum.h"

namespace Candela
{
    namespace Light
    {
        class PointLight 
            : ILight{
        public:
            PointLight(const Mathematics::Vector& point, Spectrum::ISpectrum * spectrum);

            virtual float power() const;
            virtual void radiance(const Mathematics::Ray& ray, Spectrum::ISpectrum& a_spectrum) const;

        private:
            const Mathematics::Vector point;
            Spectrum::ISpectrum * const spectrum;
        };
    }
}



#endif	/* POINTLIGHT_H */

