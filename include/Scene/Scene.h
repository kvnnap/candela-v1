/* 
 * File:   Scene.h
 * Author: Kevin
 *
 * Created on 08 September 2014, 17:22
 */

#ifndef SCENE_H
#define	SCENE_H

#include "IScene.h"
#include "../Mesh/IMesh.h"
#include "../Light/ILight.h"
#include <vector>

namespace Candela
{
    namespace Scene
    {
        class Scene 
         : public IScene{
        public:
            Scene();
            
        private:
            //lights don't need to be accelerated with a structure
            std::vector<Light::ILight*> lights_;
            
            //reference to a Mesh  - to be changed to a primitive mesh
            Mesh::IMesh * mesh_;
        };
    }
}

#endif	/* SCENE_H */

