/* 
 * File:   TriSpectrum.h
 * Author: Kevin
 *
 * Created on 17 April 2014, 19:58
 */

#ifndef TRISPECTRUM_H
#define	TRISPECTRUM_H

#include "ISpectrum.h"
#include "Mathematics/Vector3.h"

namespace Candela
{
    namespace Spectrum
    {
        class TriSpectrum
                : public ISpectrum
        {
        public:
            TriSpectrum();
            TriSpectrum(float value);
            TriSpectrum(const TriSpectrum& rgb);
            TriSpectrum(const Mathematics::Vector& rgb);
            TriSpectrum(float a, float b, float c);
            

            unsigned getNumSamples() const;

            
            void clamp(float min = 0.f, float max = 1.f);
            Mathematics::Vector& getAbcReference();
            
            virtual void add(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void subtract(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void multiply(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void multiply(float scale, ISpectrum& result) const;
            virtual void divide(const ISpectrum& spectrum, ISpectrum& result) const;
            virtual void divide(float invScale, ISpectrum& result) const;
            
            virtual ISpectrum& operator += (const ISpectrum& spectrum);
            virtual ISpectrum& operator -= (const ISpectrum& spectrum);
            virtual ISpectrum& operator *= (const ISpectrum& spectrum);
            virtual ISpectrum& operator *= (float scale);
            virtual ISpectrum& operator /= (const ISpectrum& spectrum);
            virtual ISpectrum& operator /= (float invScale);
            
        protected:
            
            Mathematics::Vector abc_;
        };
    }
}


#endif	/* TRISPECTRUM_H */

