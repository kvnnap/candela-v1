/* 
 * File:   RgbSpectrum.h
 * Author: Kevin
 *
 * Created on 26 April 2014, 13:34
 */

#ifndef RGBSPECTRUM_H
#define	RGBSPECTRUM_H

#include "TriSpectrum.h"

namespace Candela
{
    namespace Spectrum
    {   
        class RgbSpectrum 
                : public TriSpectrum
        {
        public:
            RgbSpectrum();
            RgbSpectrum(float value);
            RgbSpectrum(const RgbSpectrum& rgb);
            RgbSpectrum(const Mathematics::Vector& rgb);
            RgbSpectrum(float red, float green, float blue);
            
            virtual float getWaveLength(unsigned sampleIndex) const;
            virtual float luminance() const;
            virtual RgbSpectrum toTristimulusSRGB() const;
            virtual XyzSpectrum toTristimulusXYZ() const;
            
            void alphaCorrect();
            void inverseAlphaCorrect();
            
        private:

        };
    }
}

#endif	/* RGBSPECTRUM_H */

