/* 
 * File:   XyzSpectrum.h
 * Author: Kevin
 *
 * Created on 26 April 2014, 14:21
 */

#ifndef XYZSPECTRUM_H
#define	XYZSPECTRUM_H

#include "TriSpectrum.h"

namespace Candela
{
    namespace Spectrum
    {   
        class XyzSpectrum 
                : public TriSpectrum
        {
        public:
            XyzSpectrum();
            XyzSpectrum(float value);
            XyzSpectrum(const XyzSpectrum& xyz);
            XyzSpectrum(const Mathematics::Vector& xyz);
            XyzSpectrum(float x, float y, float z);
            
            virtual float getWaveLength(unsigned sampleIndex) const;
            virtual float luminance() const;
            virtual RgbSpectrum toTristimulusSRGB() const;
            virtual XyzSpectrum toTristimulusXYZ() const;
            
        private:

        };
    }
}

#endif	/* XYZSPECTRUM_H */

