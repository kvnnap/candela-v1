/* 
 * File:   IMesh.h
 * Author: Kevin
 *
 * Created on 14 September 2014, 15:38
 */

#ifndef IMESH_H
#define	IMESH_H

#include "../Shape/IShape.h"

namespace Candela
{
    namespace Mesh
    {
        class IMesh
            : public Shape::IShape
        {
        public:
            IMesh();
            
            //Inherited from Shape
            virtual std::string toString() const;

            //Own
            virtual void add(const Shape::IShape& shape) = 0;
            virtual bool remove(const Shape::IShape& shape) = 0;
            virtual void clear() = 0;
            
        private:
            
        };
    }
}

#endif	/* IMESH_H */

