/* 
 * File:   LinearMesh.h
 * Author: Kevin
 *
 * Created on 14 September 2014, 16:06
 */

#ifndef LINEARMESH_H
#define	LINEARMESH_H

#include "IMesh.h"
#include <vector>
#include "../Shape/AxisAlignedBoundingBox.h"
#include "../Shape/IShape.h"

namespace Candela
{
    namespace Mesh
    {
        class LinearMesh 
            : public IMesh
        {
        public:
            virtual bool intersect(const Mathematics::BoundedRay& ray, Shape::Intersection& intersection) const;
            virtual float getSurfaceArea() const;
            
            void add(const Shape::IShape& shape);
            bool remove(const Shape::IShape& shape);
            void clear();
        private:
            //Shape::AxisAlignedBoundingBox boundingBox_; add this later
            std::vector<const Shape::IShape *> meshList_;
        };
    }
}

#endif	/* LINEARMESH_H */

